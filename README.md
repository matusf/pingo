# pingo

like `ping(8)` but in go

## Installation

`pingo` uses go modules for dependency management. You can build it from source:

```sh
go build .
```

or via `go get`:

```sh
go get -u gitlab.com/matusf/pingo
```

## Usage

Note: `pingo` supports IPv4 and IPv6 addresses!

```txt
$ pingo -h
Usage: pingo [options] destination
    -c count    stop after sending count packets
    -h          print help message
    -s size     packet size (default 8)
```

## Features

- IPv4 & IPv6 support
- round trip time & loss statistics
- configurable payload size
- configurable retry count
