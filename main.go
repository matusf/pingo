package main

import (
	"bytes"
	"encoding/binary"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"time"

	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv4"
	"golang.org/x/net/ipv6"
)

var logger = log.New(os.Stderr, "", 0)

const (
	usage = `Usage: pingo [options] destination
    -c count    stop after sending count packets
    -h          print help message
    -s size     packet size (default 8)
`
	timestampSize = 8
)

type pingo struct {
	// type of network to use, udp4 or udp6
	network string

	// sequence number for next packet
	seq int

	// number of sent packets
	totalSend int

	// number of received packets
	totalReceived int

	// protocol number, ICMPv4 or ICMPv6
	proto int

	// round trip times
	rtts []float64

	isIPv4       bool
	packetSize   int
	startTime    int64
	targetDomain string
	conn         *icmp.PacketConn
	target       *net.IPAddr
	messageType  icmp.Type
}

func int64ToBytes(n int64) []byte {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(n))
	return b
}

func bytesToInt64(b []byte) int64 {
	return int64(binary.LittleEndian.Uint64(b))
}

// Send ping requests
func (p *pingo) send() error {
	time := time.Now().UnixNano()
	message := icmp.Message{
		Type: p.messageType,
		Code: 0,
		Body: &icmp.Echo{
			ID:  os.Getpid() & 0xffff,
			Seq: p.seq,
			Data: append(
				int64ToBytes(time), bytes.Repeat([]byte{'X'}, p.packetSize-timestampSize)...),
		},
	}

	payload, err := message.Marshal(nil)
	if err != nil {
		return err
	}

	dst := net.UDPAddr{IP: p.target.IP, Zone: p.target.Zone}
	if _, err := p.conn.WriteTo(payload, &dst); err != nil {
		return err
	}
	p.seq++
	p.totalSend++
	return nil
}

// Read incomming ping responses
func (p *pingo) read(done <-chan bool, wg *sync.WaitGroup) {
	var (
		err    error
		n, ttl int
		peer   net.Addr
	)
	defer wg.Done()
	for {
		select {
		case <-done:
			return
		default:
			if err := p.conn.SetReadDeadline(time.Now().Add(200 * time.Millisecond)); err != nil {
				logger.Fatalf("Unable to set dead line: %s\n", err)
			}

			buffer := make([]byte, 1024)
			if p.isIPv4 {
				var cm *ipv4.ControlMessage
				n, cm, peer, err = p.conn.IPv4PacketConn().ReadFrom(buffer)
				if _, ok := err.(*net.OpError); ok {
					continue
				}
				ttl = cm.TTL
			} else {
				var cm *ipv6.ControlMessage
				n, cm, peer, err = p.conn.IPv6PacketConn().ReadFrom(buffer)
				if _, ok := err.(*net.OpError); ok {
					continue
				}
				ttl = cm.HopLimit
			}

			receivedAt := time.Now().UnixNano()
			rm, err := icmp.ParseMessage(p.proto, buffer[:n])
			if err != nil {
				logger.Printf("Unable to parse message: %s\n", err)
				continue
			}

			switch rm.Type {
			case ipv4.ICMPTypeEchoReply, ipv6.ICMPTypeEchoReply:
				b := rm.Body.(*icmp.Echo)
				time := float64(receivedAt-bytesToInt64(b.Data[:timestampSize])) / 10e5
				p.rtts = append(p.rtts, time)
				fmt.Printf("%d bytes from %s: icmp_seq=%d ttl=%d time=%.2fms\n", n, peer, b.Seq, ttl, time)
				p.totalReceived++
			case ipv4.ICMPTypeTimeExceeded, ipv6.ICMPTypeTimeExceeded:
				fmt.Printf("From %s Time to live exceeded", peer)
			default:
				logger.Printf("invalid response type %s", rm.Type)
			}
		}
	}
}

func (p pingo) report() {
	var min, max float64
	if len(p.rtts) > 0 {
		min = p.rtts[0]
		max = p.rtts[0]
	}
	sum := 0.0
	totalTime := (time.Now().UnixNano() - p.startTime) / 10e5
	for _, time := range p.rtts {
		if time > max {
			max = time
		}
		if time < min {
			min = time
		}
		sum += time
	}
	if p.totalSend > 0 {
		fmt.Printf("\n--- %s ping statistics ---\n", p.targetDomain)
		fmt.Printf("%d packets transmitted, %d received, %d%% packet loss, time %dms\n",
			p.totalSend, p.totalReceived, 100-100*p.totalReceived/p.totalSend, totalTime)
		fmt.Printf("rtt min/avg/max = %.2f/%.2f/%.2f ms\n", min, sum/float64(p.totalSend), max)
	}
}

func newPingo(host string, packetSize int) (*pingo, error) {
	var (
		network     string
		messageType icmp.Type
		proto       int
	)
	target, err := net.ResolveIPAddr("ip", host)
	if err != nil {
		return nil, fmt.Errorf("unable to resolve: %s", host)
	}
	isIPv4 := target.IP.To4() != nil
	if isIPv4 {
		network = "udp4"
		messageType = ipv4.ICMPTypeEcho
		proto = 1
	} else {
		network = "udp6"
		messageType = ipv6.ICMPTypeEchoRequest
		proto = 58
	}
	return &pingo{
		proto:        proto,
		messageType:  messageType,
		network:      network,
		target:       target,
		targetDomain: host,
		seq:          1,
		isIPv4:       isIPv4,
		startTime:    time.Now().UnixNano(),
		packetSize:   packetSize,
	}, nil
}

func (p *pingo) listen() error {
	conn, err := icmp.ListenPacket(p.network, "")
	if err != nil {
		return fmt.Errorf("unable to connect: %s", err)
	}

	if p.isIPv4 {
		if err := conn.IPv4PacketConn().SetControlMessage(ipv4.FlagTTL, true); err != nil {
			return fmt.Errorf("unable to set TTL flag: %s", err)
		}
	} else {
		if err := conn.IPv6PacketConn().SetControlMessage(ipv6.FlagHopLimit, true); err != nil {
			return fmt.Errorf("unable to set TTL flag: %s", err)
		}
	}
	p.conn = conn
	return nil
}

func main() {
	packetSize := flag.Int("s", 8, "packet size")
	packetCount := flag.Int("c", 0, "stop after sending count packets")
	flag.Usage = func() {
		fmt.Print(usage)
	}

	flag.Parse()
	if flag.NArg() < 1 {
		flag.Usage()
		os.Exit(1)
	}

	p, err := newPingo(flag.Arg(0), *packetSize)
	if err != nil {
		logger.Fatal(err)
	}

	if err := p.listen(); err != nil {
		logger.Fatal(err)
	}
	defer p.conn.Close()

	fmt.Printf("PING %s (%s) %d bytes of data\n", p.targetDomain, p.target.IP, p.packetSize)

	var wg sync.WaitGroup
	done := make(chan bool)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			done <- true
			defer wg.Wait()
			p.report()
			os.Exit(0)
		}
	}()

	wg.Add(1)
	defer wg.Wait()
	defer close(done)
	go p.read(done, &wg)
	for {
		if err := p.send(); err != nil {
			logger.Println(err)
		}
		if *packetCount > 0 && p.totalSend >= *packetCount {
			done <- true
			p.report()
			break
		}
		time.Sleep(1 * time.Second)
	}
}
